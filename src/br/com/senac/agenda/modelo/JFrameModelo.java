
package br.com.senac.agenda.modelo;

import javax.swing.JFrame;
import javax.swing.JOptionPane;





public abstract class JFrameModelo extends JFrame {

    public JFrameModelo() {
         this.setVisible(true);
    }

    
    
    
    public void showMensagemInformacao(String messagem) {
        JOptionPane.showMessageDialog(this, messagem, "Informação", JOptionPane.INFORMATION_MESSAGE);
    }
    
     public void showMensagemAdvertencia(String messagem) {
        JOptionPane.showMessageDialog(this, messagem, "Informação", JOptionPane.WARNING_MESSAGE);
    }
     
      public void showMensagemErro(String messagem) {
        JOptionPane.showMessageDialog(this, messagem, "Informação", JOptionPane.ERROR_MESSAGE);
    }
      
      public boolean  isNotEmpty(String txt){
          return !txt.isEmpty();      }
              

}
